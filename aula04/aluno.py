class Aluno:
    nome = ''
    matricula = 0
    notas = []
    
    def imprimir(self):
        matricula = 3
        print("{} - {}".format(self.matricula, self.nome))
        
    def addNota(self, n):
        self.notas.append(n)
        
    def calcularMedia(self):
        soma = 0
        for n in self.notas:
            soma = soma + n
        return soma / len(self.notas)