## Atividade

Criar um programa em python que leia os seguintes atributos: nome, tempo de empresa, endereço e salário e sexo e armazenar em lista. O programa deve ler enquanto o usuário quiser cadastrar.
Caso a pessoa tenha mais de 2 anos de empresa e receba menos de R$ 2000 o salário dela deve ser aumentado em 25%.

Após o usuário decidir parar de armazenar e o programa calcular os aumentos ele deve imprimir uma mensagem informando:

```bash
Prezado <José>.
Você possui <3> anos de empresa.
Seu <novo> salário é <>:
```
Obs.: entre <> são variáveis que dependerão da entrada do usuário.

Sugestão mudar a concordância do adjetivo Prezado de acordo com o sexo da pessoa.