# Atividade


Criar uma classe chamada *Viagem* com os atributos *data* como string, *destino* como string, e *viajantes* como lista de strings. 

Criar uma lista de Viagem e adicionar 5 viagens nela.

Realizar o commit no gitlab.

Basear no exemplo:

https://gitlab.com/senai-fatesg/2019-01-progredes/professor/blob/master/aula04/exemplo_classe.ipynb

