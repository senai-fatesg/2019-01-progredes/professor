from flask import render_template
from flask import Flask


app = Flask(__name__, static_url_path='')


@app.route('/')
def login():
    return app.send_static_file('form.html')

if __name__ == "__main__":
    app.run(debug=True,port=1234)    