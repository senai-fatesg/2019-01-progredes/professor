from flask import Flask, render_template

app = Flask(__name__)

@app.route('/hello')
def hello():
    user = {'username': 'Francisco'}
    return render_template('hello.html', title='Home', user=user)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True,port=8080)    