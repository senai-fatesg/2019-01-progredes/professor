from flask import Flask
from flask import request
import ceplib as cep


app = Flask(__name__)

@app.route("/cep")
def getCep():
    cepcons = request.args.get('cep')
    return cep.consultaCep(cepcons)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True,port=8080)