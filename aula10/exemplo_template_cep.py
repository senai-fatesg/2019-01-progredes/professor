from flask import Flask, render_template, request
import ceplib as ceplib

app = Flask(__name__)

@app.route("/cep")
def getCep():
    cepcons = request.args.get('cep')
    cep = ceplib.consultaCep(cepcons)
    return render_template('cep.html', cep=cep)

@app.route("/")
def getCepForm():
    return render_template('cepform.html')

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True,port=8080)    