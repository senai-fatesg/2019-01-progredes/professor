# Repositório do professor

## Lista de usuários e alunos

https://docs.google.com/spreadsheets/d/1bBtBM2enLMb0pUZ5cVo7NOAnu65j04YC7wgCKKpyZrc/edit#gid=0

## Comandos básicos do GIT

### Para baixar o projeto pela primeira vez:

```bash
git clone <repositorio>
cd <diretório>
```

### Para subir as alterações para o gitlab:

```bash
git add *
git commit -am "uma mensagem qualquer"
git push
```

### Para baixar as alterações para o gitlab:

```bash
git pull
```