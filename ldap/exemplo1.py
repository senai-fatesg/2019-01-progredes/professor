#https://www.lfd.uci.edu/~gohlke/pythonlibs/#python-ldap
#pip install python_ldap-3.2.0-cp37-cp37m-win_amd64.whl

import sys, ldap,ldap.asyncsearch


s = ldap.asyncsearch.List(ldap.initialize('ldap://localhost'),)

s.startSearch('dc=stroeder,dc=com',ldap.SCOPE_SUBTREE,'(objectClass=*)',)

try:
  partial = s.processResults()
except ldap.SIZELIMIT_EXCEEDED:
  sys.stderr.write('Warning: Server-side size limit exceeded.\n')
else:
  if partial:
    sys.stderr.write('Warning: Only partial results received.\n')

sys.stdout.write(
  '%d results received.\n' % (
    len(s.allResults)
  )
)
